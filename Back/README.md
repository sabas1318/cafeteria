# Primer paso
# Activar un entorno virtual
- una ves dentro de la carpeta Back
- python -m venv venv
- env\Scripts\activate.bat
# Segundo paso
## Instalar Requerimientos
- pip install -r requirements.txt
# Tercer Paso
## Activar la base de datos
- python manage.py migrate
- python manage.py makemigrations
# Ultimo paso
## Activar el servidor
- python manage.py runserver
- Abrir el navegador en la direccion `http://127.0.0.1:8000`