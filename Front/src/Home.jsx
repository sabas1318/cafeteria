import {useState, useEffect} from 'react';
import Productos from './Productos';
import Pedidos from './Pedidos';
import Usuarios from './Usuarios';
import {ToastContainer, toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Home = ({onLogout, userId}) => {
    const [user, setUser] = useState();
    const [user_name, setUser_name] = useState(""); 
    const [showPedidos, setShowPedidos] = useState(false);
    const [showProductos, setShowProductos] = useState(false);
    const [showUsuarios, setShowUsuarios] = useState(false);

    useEffect(() => {
        fetch('http://localhost:8000/usuarios/' + userId, {
            method: 'GET' /* or POST/PUT/PATCH/DELETE */,
            headers: {
                Authorization: `Bearer ${JSON.parse(window.localStorage.getItem('accessToken'))}`,
                'Content-Type': 'application/json',
            },
        })
            .then((res) => res.json())
            .then((userData) => {
                setUser(userData);
                setUser_name(userData.data.username);
                console.log("El usuario =>", userData.data.username);
            })
    }, []);

    const logoutHandler = () => {
        onLogout();
    };

    const togglePedidos = () => {

        setShowPedidos(!showPedidos);
    };

    const toggleUsuarios = () => {

        setShowUsuarios(!showUsuarios);
    };

    const toggleProductos = () => {
        setShowProductos(!showProductos);
    };

    const ocultarProductos = () => {
        setShowProductos(false);
    };

    const ocultarPedidos = () => {
        setShowPedidos(false);
    };
    return (
        <> 
                {user && (
                    <>
                    <button className="boton-ini" onClick={logoutHandler}>Salir</button>
                        <h1 style={{color: '#FFF'}}>Bienvenido {user_name}</h1>
                        <br />
                    <div className="button-container">
                     <button className="boton boton-inicio" onClick={() => {toggleProductos();ocultarPedidos(),setShowUsuarios(false);}}>Productos</button>
                     <button className="boton boton-inicio" onClick={() => {toggleUsuarios(),ocultarProductos(),ocultarPedidos();}}>Usuarios</button>
                     <button className="boton boton-inicio" onClick={() => {togglePedidos();ocultarProductos(),setShowUsuarios(false);}}>Pedidos</button>
                    </div>
                    <div className="container">
                        {showProductos && <Productos />}
                    </div>
                    <div className="container">
                        {showPedidos && <Pedidos />}
                    </div>
                    <div className="container">
                        {showUsuarios && <Usuarios />}
                    </div>
                    </>
                )}        
        </>
      );    
};
export default Home;