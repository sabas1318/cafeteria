import { useState } from 'react'
import jwtDecode from 'jwt-decode'
import {ToastContainer, toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Login = ({ onLogin }) => {
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const loginHandle = (e) => {
    e.preventDefault()
// Inicio de session con JWT token
    fetch('http://localhost:8000/api/token/', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        username,
        password,
      }),
    })
      .then((res) => res.json())
      .then((tokenData) => {
        window.localStorage.setItem('accessToken', JSON.stringify(tokenData.access))
        console.log(tokenData);
        console.log(jwtDecode(tokenData.access).user_id);
        onLogin(jwtDecode(tokenData.access).user_id)
      })
  }

  return (
    <form onSubmit={loginHandle}>
      <h1>Coffee Time</h1>
      <input
        style={{height: "50px",width: "300px"}}
        aria-label="Username"
        placeholder="Usuario"
        id="username"
        type="text"
        onChange={(e) => {
          setUsername(e.target.value)
        }}
      />
      <input
      style={{height: "50px",width: "300px"}}
        aria-label="Password"
        placeholder="Contraseña"
        id="password"
        type="password"
        onChange={(e) => {
          setPassword(e.target.value)
        }}
      />
      <button type="submit">Inicio</button>
    </form>
  )
}

export default Login