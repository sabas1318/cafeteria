El primer paso es activar el backend para empezar
debemos activar el entorno virtual con los siguientes
comandos:
'python -m venv env' 
este tarda un momento ya que empieza a crear las 
carpetas que son necesarias para levantar el proyecto 
'env\Scripts\activate.bat'
con este comando activamos el entorno virtual ya solo
necesitamos instalar algunos componentes utilizando el 
siguiente comando:
'pip install -r requirements.txt'
luego debemos actualizar la base de datos para que no nos
genere ningun error este paso se realiza con dos comandos
'python manage.py migrate'
y luego
'python manage.py makemigrations'
ahora solo nos queda activar el servidor con
'python manage.py runserver'
el cual levanta el proyecto y nos proporciona una direccion
que normalmente es `http://127.0.0.1:8000`

Ahora solo nos queda el frontend para activar necesitamos instalar
las dependencias de node con el comando
'npm install node'
una vez que se descargan todos los recursos necesarios
podemos levantar el servidor con el siguiente comando
'npm run dev'
esperamos a que cargue y nos genere una direccion
'http://localhost:5173/'